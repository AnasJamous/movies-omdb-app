import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AuthService } from '../services/auth.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add authorization header with jwt token if user is logged in 
    const currentUser = this.authService.currentUserValue;
    if (currentUser && currentUser.token) {
      if (request.url.includes('/users/')) {
        request = request.clone({
          setHeaders: {
            Authorization: `Bearer ${currentUser.token}`
          }
        });
      }
    }
    return next.handle(request);
  }
}

//  The implementation resource from this link :
//  src: "https://jasonwatmore.com/post/2018/11/16/angular-7-jwt-authentication-example-tutorial"